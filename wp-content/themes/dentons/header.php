<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dentons
 */

?>
<!doctype html>
<html prefix="og: http://ogp.me/ns#" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="description" content="See the bigger picture. Have the big ideas. Make the big deals.">
  	<meta name="author" content="Tonic Agency">
		<?php if( have_rows('seo') ): while ( have_rows('seo') ) : the_row(); ?>
			<meta property="og:image" content="<?php the_sub_field('image'); ?>" />
			<meta property="og:description" content="<?php the_sub_field('description'); ?>" />
			<meta name="keywords" content="<?php the_sub_field('keywords'); ?>" />
		<?php endwhile; endif; ?>

	<meta property="og:url" content="<?php echo home_url($wp->request); ?>" />
	<meta property="og:type" content="website" />

	<link rel="icon" type="image/ico" href="https://www.dentons.com/favicon.ico">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://challengers.dentons.com/wp-content/themes/dentons/css/reset.css">
    <link rel="stylesheet" href="https://challengers.dentons.com/wp-content/themes/dentons/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

	<script src="https://challengers.dentons.com/wp-content/themes/dentons/js/modernizr.js"></script>
	<?php wp_head(); ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-147969023-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	  gtag('config', 'UA-147969023-1');
	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-W5J29M6');</script>
	<!-- End Google Tag Manager -->
</head>


<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W5J29M6"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<?php
					$displayMenu = "primary-menu";
					$templateUsed = get_page_template_slug( $post->ID );
					if ($templateUsed == "apprentices-template.php") $displayMenu = "Menu Apprentices";
					if ($templateUsed == "trainees-template.php") $displayMenu = "Menu Trainees";
					if ($templateUsed == "me-template.php") $displayMenu = "Menu ME";
					if ($templateUsed == "home-template.php") $displayMenu = "Menu Home";
				?>
	<!--Start Mobile navigation-->
                <ul class="sidenav" id="mobile-demo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="brand-logo"><img src="https://challengers.dentons.com/wp-content/themes/dentons/img/logo.png" alt="logo" /></a>
					<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu'        => $displayMenu,
					) );
					?>
                </ul>
				<!--End Mobile navigation-->
    <main class="cd-main <?php wp_title(''); ?>">
        <section class="cd-section index visible">

			<!--Start Transition-->
            <header>
                <div class="cd-title"></div>
            </header>
			<!--End Transition-->

			<div class="main-content">


				<!--Start Main navigation-->
                <nav class="cd-side-navigation z-depth-0">
                    <div class="nav-wrapper">

                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="brand-logo"><img src="https://challengers.dentons.com/wp-content/themes/dentons/img/logo.png" alt="logo" /></a> <a data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                        <ul class="right hide-on-med-and-down">

							<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-1',
								'menu'        => $displayMenu,
							) );
							?>
                        </ul>
                    </div>

                </nav>
				<!--End Main navigation-->
