<?php
/**
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package dentons
 */

get_header();
?>

<?php /* Template Name: Apprentices Template */ ?>

	<!--Page content-->
	<?php if ( have_posts() ) : while  ( have_posts() ) : the_post();   ?>

	<!--<div class="main-content">-->

		<!--Start Hero-->
		<?php if( have_rows('hero') ): while ( have_rows('hero') ) : the_row(); ?>
		<style>
			.hero-content {
				background-image: url('<?php the_sub_field('background_image'); ?>')
			}
			@media only screen and (max-width: 750px) {
				.hero-content {
					background-image: url('<?php the_sub_field('mobile-background_image'); ?>') !important
				}
			}
		</style>
		<div class="hero-content">
			<div class="container">
			<div style="margin-bottom: 10vh" class="row">
				<div class="col s6 header-copy">
					<h2><span>CHALLENGERS.</span></h2>
					<?php if( have_rows('text_lines') ): while ( have_rows('text_lines') ) : the_row(); ?>
					<p><span><?php the_sub_field('lines'); ?></span></p>
					<?php endwhile; endif; ?>
					<h2><span>ACCEPTED.</span></h2>
				</div>
				<div class="col s6 intro-copy">
					<h4><span><?php the_sub_field('intro_header'); ?></span></h4>
					<div class="intro-copy-block"><?php the_sub_field('intro_copy'); ?></div>
				</div>
			</div>
		</div>
			</div>
		<?php endwhile; endif; ?>
		<!--End Hero-->




		<!--Start Main Content-->
                <div style="background-image: url('<?php the_field('content_background'); ?>'); background-color: <?php the_field('content_background_colour'); ?> " class="main-content-area">
                    <div class="container">

						<!--Start heading and copy-->
						<?php if( have_rows('header_and_copy') ): while ( have_rows('header_and_copy') ) : the_row(); ?>
				 		<div class="row">
                        	<div class="col s12">
								<h5 style="color: #fff; background-color: <?php the_sub_field('header_colour'); ?>" class="heading"><?php the_sub_field('header'); ?></h5>
								<div style="background-color:<?php the_sub_field('copy-bgc'); ?>" class="copy-block"><?php the_sub_field('copy'); ?></div>
							</div>
						</div>
						<?php endwhile; endif; ?>
						<!--End heading and copy-->

						<!--Start Accordion-->
						<?php
						$url = $_SERVER['QUERY_STRING'];
						$totalitems = 0;
						if( have_rows('accordion') ): while ( have_rows('accordion') ) : the_row(); ?>
                        <div class="row">
                            <div class="col s12">
                                <ul class="collapsible z-depth-0 ">
									<?php if( have_rows('item') ): while ( have_rows('item') ) : the_row(); ?>
									 <li<?php
										if ($url==get_sub_field('accordion_id')) {
											echo ' class="active"';
										}
										if (($url=="") && ($totalitems==0)) {
											// Make the first one active by default
											echo ' class="active"';
										}
										?>>
                                        <div id="<?php the_sub_field('accordion_id'); ?>" class="collapsible-header "><?php the_sub_field('header'); ?> <i class="material-icons">add</i></div>
                                        <div class="collapsible-body"><span><?php the_sub_field('copy'); ?></span></div>
                                    </li>
									<?php endwhile; endif; ?>
                                </ul>
                            </div>
                        </div>
						<?php
						$totalitems++;
						endwhile; endif; ?>
						<!--End Accordion-->

						<!--Start heading and copy 2-->
						<?php if( have_rows('header_and_copy2') ): while ( have_rows('header_and_copy2') ) : the_row(); ?>
				 		<div class="row">
                        	<div class="col s12">
								<h5 style="color: #fff; background-color: <?php the_sub_field('header_colour'); ?>" class="heading"><?php the_sub_field('header'); ?></h5>
								<div style="background-color:<?php the_sub_field('copy-bgc'); ?>" class="copy-block"><?php the_sub_field('copy'); ?></div>
							</div>
						</div>
						<?php endwhile; endif; ?>
						<!--End heading and copy 2-->

						<!--Start infographic-->
						<?php if( have_rows('infographic') ): while ( have_rows('infographic') ) : the_row(); ?>
						<div class="row">
                        	<div class="col s12">
								<h5 style="color: #fff; background-color: #6E2D91" class="heading"><?php the_sub_field('heading'); ?></h5>
								<div class="infographic-group">

									<?php if( have_rows('item') ): while ( have_rows('item') ) : the_row(); ?>
									<div class="col s2">
										<?php the_sub_field('copy'); ?>
									</div>
									<?php endwhile; endif; ?>

								</div>
							</div>
						</div>
						<?php endwhile; endif; ?>
						<!--End infographic-->
					</div>


						<!--Start Carousel-->
						<?php if( have_rows('carousel') ): while ( have_rows('carousel') ) : the_row(); ?>
						<div class="row">
							<div class="col s12 no-padding">
								<div class="carousel carousel-slider center">
								<?php if( have_rows('item') ): while ( have_rows('item') ) : the_row(); ?>
									<div style="background-image: url('<?php the_sub_field('background_image'); ?>')" class="carousel-item <?php the_sub_field('name'); ?>">
										<div class="col s6 left-align">
											<h5 class="heading"><?php the_sub_field('name'); ?></h5>
										</div>
										<div class="col s6 right-align">
											<?php the_sub_field('copy'); ?>
											<?php the_sub_field('position'); ?>
										</div>
									</div>
								<?php endwhile; endif; ?>
								</div>
							</div>
						</div>
						<?php endwhile; endif; ?>
						<!--End Carousel-->


                </div>
				<!--End Main Content-->


<?php endwhile; endif; ?>
<!--End Page content-->

<?php get_footer(dark); ?>
