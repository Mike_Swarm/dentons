<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package dentons
 */

get_header();
?>

<style>body {
  background: black;
  font-family: 'Varela', sans-serif;
}

.glitch {
    color: #000000;
    font-size: 200px;
    position: relative;
    width: 400px;
    margin: 0 auto;
    line-height: initial !important;
    font-weight: bold;
}

@keyframes noise-anim {
  0% {
    clip: rect(8px, 9999px, 91px, 0);
  }
  5% {
    clip: rect(32px, 9999px, 44px, 0);
  }
  10% {
    clip: rect(53px, 9999px, 47px, 0);
  }
  15% {
    clip: rect(48px, 9999px, 30px, 0);
  }
  20% {
    clip: rect(87px, 9999px, 98px, 0);
  }
  25% {
    clip: rect(25px, 9999px, 19px, 0);
  }
  30% {
    clip: rect(83px, 9999px, 77px, 0);
  }
  35% {
    clip: rect(93px, 9999px, 85px, 0);
  }
  40% {
    clip: rect(86px, 9999px, 7px, 0);
  }
  45% {
    clip: rect(39px, 9999px, 52px, 0);
  }
  50% {
    clip: rect(94px, 9999px, 46px, 0);
  }
  55% {
    clip: rect(80px, 9999px, 39px, 0);
  }
  60% {
    clip: rect(21px, 9999px, 18px, 0);
  }
  65% {
    clip: rect(34px, 9999px, 84px, 0);
  }
  70% {
    clip: rect(42px, 9999px, 35px, 0);
  }
  75% {
    clip: rect(33px, 9999px, 41px, 0);
  }
  80% {
    clip: rect(87px, 9999px, 66px, 0);
  }
  85% {
    clip: rect(74px, 9999px, 74px, 0);
  }
  90% {
    clip: rect(58px, 9999px, 3px, 0);
  }
  95% {
    clip: rect(16px, 9999px, 57px, 0);
  }
  100% {
    clip: rect(71px, 9999px, 5px, 0);
  }
}
.glitch:after {
  content: attr(data-text);
  position: absolute;
  left: 2px;
  text-shadow: -1px 0 red;
  top: 0;
  color: white;
  background: black;
  overflow: hidden;
  clip: rect(0, 900px, 0, 0);
  animation: noise-anim 2s infinite linear alternate-reverse;
}

@keyframes noise-anim-2 {
  0% {
    clip: rect(46px, 9999px, 5px, 0);
  }
  5% {
    clip: rect(71px, 9999px, 59px, 0);
  }
  10% {
    clip: rect(46px, 9999px, 77px, 0);
  }
  15% {
    clip: rect(73px, 9999px, 75px, 0);
  }
  20% {
    clip: rect(81px, 9999px, 47px, 0);
  }
  25% {
    clip: rect(18px, 9999px, 33px, 0);
  }
  30% {
    clip: rect(85px, 9999px, 75px, 0);
  }
  35% {
    clip: rect(55px, 9999px, 96px, 0);
  }
  40% {
    clip: rect(41px, 9999px, 70px, 0);
  }
  45% {
    clip: rect(62px, 9999px, 15px, 0);
  }
  50% {
    clip: rect(94px, 9999px, 84px, 0);
  }
  55% {
    clip: rect(66px, 9999px, 20px, 0);
  }
  60% {
    clip: rect(10px, 9999px, 66px, 0);
  }
  65% {
    clip: rect(29px, 9999px, 46px, 0);
  }
  70% {
    clip: rect(54px, 9999px, 28px, 0);
  }
  75% {
    clip: rect(58px, 9999px, 87px, 0);
  }
  80% {
    clip: rect(29px, 9999px, 35px, 0);
  }
  85% {
    clip: rect(49px, 9999px, 73px, 0);
  }
  90% {
    clip: rect(68px, 9999px, 31px, 0);
  }
  95% {
    clip: rect(84px, 9999px, 95px, 0);
  }
  100% {
    clip: rect(43px, 9999px, 82px, 0);
  }
}
.glitch:before {
  content: attr(data-text);
  position: absolute;
  left: -2px;
  text-shadow: 1px 0 blue;
  top: 0;
  color: white;
  background: black;
  overflow: hidden;
  clip: rect(0, 900px, 0, 0);
  animation: noise-anim-2 3s infinite linear alternate-reverse;
}
	
	
	
	.menu, a.sidenav-trigger, .main-footer-container {
		display: none
	}
	
	

	video#hero {
		z-index: -1;
	-webkit-filter: grayscale(100%);
    filter: grayscale(100%);
	}
	
</style>
<div style="align-items: center; background-size: cover; height: 100vh !important" class="hero-content">
	<div class="container">		
	<div class="row">
				
				<div class="col s6">
					<div class="glitch" data-text="404">404</div> 
				</div>
				<div class="col s6 header-copy">
					<h2><span>CHALLENGERS.</span></h2>
					<h2><span>Rejected. :(</span></h2>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="waves-effect waves-light btn-large">Try again</a>
				</div>
			</div>
		</div>
	
</div>
 <video playsinline autoplay="" loop="" muted="" preload="auto" id="hero">
					<source src="https://tonic-agency.com/projects/dentons/media/Dentons.mp4" type="video/mp4">
				</video>

<?php
get_footer();
