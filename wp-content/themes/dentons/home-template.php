<?php
/**
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package dentons
 */

get_header();
?>

<?php /* Template Name: Home Template */ ?>

	<!--Page content-->
	<?php if ( have_posts() ) : while  ( have_posts() ) : the_post();   ?>

	<div class="landing">





				<!--Start Hero-->
		<?php if( have_rows('hero') ): while ( have_rows('hero') ) : the_row(); ?>
                <div class="container hero-content">
                    <div class="row">
                        <div class="col s6 header-copy">
							<h2><span>CHALLENGERS.</span></h2>
							<?php if( have_rows('text_lines') ): while ( have_rows('text_lines') ) : the_row(); ?>
							<p><span><?php the_sub_field('lines'); ?></span></p>
							<?php endwhile; endif; ?>
							<h2><span>ACCEPTED.</span></h2>
						</div>
                        <div class="col s6 intro-copy">
                            <div class="play-button">
								<a id="play" class="waves-effect waves-light modal-trigger" href="#modal1"><i class="material-icons">play_circle_filled</i></a>
							</div>
                        </div>
                    </div>
                </div>
                <video playsinline autoplay="" loop="" muted="" preload="auto" id="hero">
					<source src="<?php the_sub_field('background_video_mobile'); ?>" type="video/mp4" media="all and (max-width: 580px)">
					<source src="<?php the_sub_field('background_video'); ?>" type="video/mp4">
				</video>
				<div id="modal1" class="modal">
					<div class="modal-content">
					  <video id="popupvideo" poster="<?php the_sub_field('placeholder'); ?>" controls width="100%">
						<source src="<?php the_sub_field('popup_video'); ?>" type="video/mp4">
					</video>
					</div>
				  </div>
		<?php endwhile; endif; ?>
				<!--End Hero-->

<?php endwhile; endif; ?>
<!--End Page content-->

<?php get_footer(); ?>
