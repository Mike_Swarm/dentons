function Check_Version() {
  var rv = -1; // Return value assumes failure.

  if (navigator.appName == 'Microsoft Internet Explorer') {

    var ua = navigator.userAgent,
      re = new RegExp("MSIE ([0-9]{1,}[\\.0-9]{0,})");

    if (re.exec(ua) !== null) {
      rv = parseFloat(RegExp.$1);
    }
  } else if (navigator.appName == "Netscape") {
    /// in IE 11 the navigator.appVersion says 'trident'
    /// in Edge the navigator.appVersion does not say trident
    if (navigator.appVersion.indexOf('Trident') === -1) rv = 12;
    else rv = 11;
  }

  return rv;
}

$(document).ready(function() {
  $('.sidenav').sidenav();
  $('.dropdown-trigger').dropdown();
  $('.collapsible').collapsible();
  // $('.collapsible').collapsible("open",0);
  $('.carousel.carousel-slider').carousel({
    fullWidth: true,
    indicators: true
  });
  $('.modal').modal();
  $('a[href*=""]').click(function(e) {
    e.preventDefault();
    newLocation = this.href;
    $('body').fadeOut('slow', newpage);
  });

  function newpage() {
    window.location = newLocation;
  }
  $('.FAQs .collapsible-body').attr('style', function(i, style) {
    return style && style.replace(/display[^;]+;?/g, '');
  });

  $("#cookie-law-info-again").wrap("<div class='container'></div>");
});

$(function() {
  $(document).delegate('#play', 'click', function() {
    $('#popupvideo')[0].play();
  });
  $(document).delegate('.modal-overlay', 'click', function() {
    $('#popupvideo')[0].pause();
  });
});

jQuery(document).ready(function($) {
  //set some variables
  var isAnimating = false,
    firstLoad = false,
    newScaleValue = 1;

  //cache DOM elements
  var dashboard = $('.cd-side-navigation'),
    mainContent = $('.cd-main'),
    loadingBar = $('#cd-loading-bar');



  //detect the 'popstate' event - e.g. user clicking the back button
  $(window).on('popstate', function() {
    if (firstLoad) {
      /*
      Safari emits a popstate event on page load - check if firstLoad is true before animating
      if it's false - the page has just been loaded
      */
      var newPageArray = location.pathname.split('/'),
        //this is the url of the page to be loaded
        newPage = newPageArray[newPageArray.length - 1].replace('.html', '');
      if (!isAnimating) triggerAnimation(newPage, false);
    }
    firstLoad = true;
  });

  //scroll to content if user clicks the .cd-scroll icon
  mainContent.on('click', '.cd-scroll', function(event) {
    event.preventDefault();
    var scrollId = $(this.hash);
    $(scrollId).velocity('scroll', {
      container: $(".cd-section")
    }, 200);
  });

  //fade parallax scrolling
  // if ($(".main-content-area")[0]) {
  //   var mainContent = $(".main-content-area").offset().top;
  //   $(window).scroll(function() {
  //     if (window.pageYOffset > 0) {
  //       $(".hero-content").css("background-color", "rgba(255,255,255," + (window.pageYOffset / mainContent) + ")");
  //     }
  //   });
  // }



  //start animation
  function triggerAnimation(newSection, bool) {
    isAnimating = true;
    newSection = (newSection == '') ? 'index' : newSection;

    //update dashboard
    dashboard.find('*[data-menu="' + newSection + '"]').addClass('selected').parent('li').siblings('li').children('.selected').removeClass('selected');
    //trigger loading bar animation
    initializeLoadingBar(newSection);
    //load new content
    loadNewContent(newSection, bool);
  }

  function initializeLoadingBar(section) {
    var selectedItem = dashboard.find('.selected'),
      barHeight = selectedItem.outerHeight(),
      barTop = selectedItem.offset().top,
      windowHeight = $(window).height(),
      maxOffset = (barTop + barHeight / 2 > windowHeight / 2) ? barTop : windowHeight - barTop - barHeight,
      scaleValue = ((2 * maxOffset + barHeight) / barHeight).toFixed(3) / 1 + 0.001;

    //place the loading bar next to the selected dashboard element
    loadingBar.data('scale', scaleValue).css({
      height: barHeight,
      top: barTop
    }).attr('class', '').addClass('loading ' + section);
  }

  function loadNewContent(newSection, bool) {
    setTimeout(function() {
      //animate loading bar
      loadingBarAnimation();

      //create a new section element and insert it into the DOM
      var section = $('<section class="cd-section overflow-hidden ' + newSection + '"></section>').appendTo(mainContent);
      //load the new content from the proper html file
      section.load(newSection + '.html .cd-section > *', function(event) {
        //finish up the animation and then make the new section visible
        var scaleMax = loadingBar.data('scale');

        loadingBar.velocity('stop').velocity({
          scaleY: scaleMax
        }, 400, function() {
          //add the .visible class to the new section element -> it will cover the old one
          section.prev('.visible').removeClass('visible').end().addClass('visible').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
            resetAfterAnimation(section);
          });

          //if browser doesn't support transition
          if ($('.no-csstransitions').length > 0) {
            resetAfterAnimation(section);
          }

          var url = newSection + '.html';

          if (url != window.location && bool) {
            //add the new page to the window.history
            //if the new page was triggered by a 'popstate' event, don't add it
            window.history.pushState({
              path: url
            }, '', url);
          }
        });
      });

    }, 50);
  }

  function loadingBarAnimation() {
    var scaleMax = loadingBar.data('scale');
    if (newScaleValue + 1 < scaleMax) {
      newScaleValue = newScaleValue + 1;
    } else if (newScaleValue + 0.5 < scaleMax) {
      newScaleValue = newScaleValue + 0.5;
    }

    loadingBar.velocity({
      scaleY: newScaleValue
    }, 100, loadingBarAnimation);
  }

  function resetAfterAnimation(newSection) {
    //once the new section animation is over, remove the old section and make the new one scrollable
    newSection.removeClass('overflow-hidden').prev('.cd-section').remove();
    isAnimating = false;
    //reset your loading bar
    resetLoadingBar();
  }

  function resetLoadingBar() {
    loadingBar.removeClass('loading').velocity({
      scaleY: 1
    }, 1);
  }
});

(function slideIn($) {

  /**
   * Copyright 2012, Digital Fusion
   * Licensed under the MIT license.
   * http://teamdf.com/jquery-plugins/license/
   *
   * @author Sam Sehnert
   * @desc A small plugin that checks whether elements are within
   *     the user visible viewport of a web browser.
   *     only accounts for vertical position, not horizontal.
   */

  $.fn.visible = function(partial) {

    var $t = $(this),
      $w = $(window),
      viewTop = $w.scrollTop(),
      viewBottom = viewTop + $w.height(),
      _top = $t.offset().top,
      _bottom = _top + $t.height(),
      compareTop = partial === true ? _bottom : _top,
      compareBottom = partial === true ? _top : _bottom;

    return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

  };

})(jQuery);

var win = $(window);

var allMods = $(".module, h5, .copy-block, .collapsible li, .infographic-group, .carousel .col.s6.right-align");

allMods.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible");
  }
});

win.scroll(function(event) {

  allMods.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("come-in");
    }
  });

});

//cursor

var cursor = {
  delay: 8,
  _x: 0,
  _y: 0,
  endX: (window.innerWidth / 2),
  endY: (window.innerHeight / 2),
  cursorVisible: true,
  cursorEnlarged: false,
  $dot: document.querySelector('.cursor-dot'),
  $outline: document.querySelector('.cursor-dot-outline'),

  init: function() {
    // Set up element sizes
    this.dotSize = this.$dot.offsetWidth;
    this.outlineSize = this.$outline.offsetWidth;

    this.setupEventListeners();
    this.animateDotOutline();
  },

  setupEventListeners: function() {
    var self = this;

    // Anchor hovering
    document.querySelectorAll('a, button, .menu-item').forEach(function(el) {
      el.addEventListener('mouseover', function() {
        self.cursorEnlarged = true;
        self.toggleCursorSize();
      });
      el.addEventListener('mouseout', function() {
        self.cursorEnlarged = false;
        self.toggleCursorSize();
      });
    });

    // Click events
    document.addEventListener('mousedown', function() {
      self.cursorEnlarged = true;
      self.toggleCursorSize();
    });
    document.addEventListener('mouseup', function() {
      self.cursorEnlarged = false;
      self.toggleCursorSize();
    });

    document.addEventListener('mousemove', function(e) {
      // Show the cursor
      self.cursorVisible = true;
      self.toggleCursorVisibility();

      // Position the dot
      self.endX = e.pageX;
      self.endY = e.pageY;
      self.$dot.style.top = self.endY + 'px';
      self.$dot.style.left = self.endX + 'px';
    });

    // Hide/show cursor
    document.addEventListener('mouseenter', function(e) {
      self.cursorVisible = true;
      self.toggleCursorVisibility();
      self.$dot.style.opacity = 1;
      self.$outline.style.opacity = 1;
    });

    document.addEventListener('mouseleave', function(e) {
      self.cursorVisible = true;
      self.toggleCursorVisibility();
      self.$dot.style.opacity = 0;
      self.$outline.style.opacity = 0;
    });
  },

  animateDotOutline: function() {
    var self = this;

    self._x += (self.endX - self._x) / self.delay;
    self._y += (self.endY - self._y) / self.delay;
    self.$outline.style.top = self._y + 'px';
    self.$outline.style.left = self._x + 'px';

    requestAnimationFrame(this.animateDotOutline.bind(self));
  },

  toggleCursorSize: function() {
    var self = this;

    if (self.cursorEnlarged) {
      self.$dot.style.transform = 'translate(-50%, -50%) scale(0.75)';
      self.$outline.style.transform = 'translate(-50%, -50%) scale(1.5)';
      self.$outline.style.background = 'rgba(0, 165, 154, 0.8196078431372549)';
      self.$dot.style.background = 'rgb(0, 165, 154)';
    } else {
      self.$dot.style.transform = 'translate(-50%, -50%) scale(1)';
      self.$outline.style.transform = 'translate(-50%, -50%) scale(1)';
      self.$outline.style.background = 'rgba(87, 39, 149, 0.7)';
      self.$dot.style.background = 'rgb(87, 39, 149)';
    }
  },

  toggleCursorVisibility: function() {
    var self = this;

    if (self.cursorVisible) {
      self.$dot.style.opacity = 1;
      self.$outline.style.opacity = 1;
    } else {
      self.$dot.style.opacity = 0;
      self.$outline.style.opacity = 0;
    }
  }
}

// cursor.init();