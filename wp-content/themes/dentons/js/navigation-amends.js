"use strict";

if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}

// Source: https://github.com/jserz/js_piece/blob/master/DOM/ParentNode/prepend()/prepend().md
(function (arr) {
  arr.forEach(function (item) {
    if (item.hasOwnProperty('prepend')) {
      return;
    }
    Object.defineProperty(item, 'prepend', {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function prepend() {
        var argArr = Array.prototype.slice.call(arguments),
          docFrag = document.createDocumentFragment();
        
        argArr.forEach(function (argItem) {
          var isNode = argItem instanceof Node;
          docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
        });
        
        this.insertBefore(docFrag, this.firstChild);
      }
    });
  });
})([Element.prototype, Document.prototype, DocumentFragment.prototype]);

( function() {
	var containers, button, menu, links, i, len;

	//Find the menus
	containers = document.querySelectorAll( '.menu' );
	if ( ! containers || containers == null) {
		return;
	}

	//Go through each menu
	containers.forEach(function(container) {

		//Find the "buttons"
		var buttons = container.querySelectorAll( '.no-click' );
		if ( 'undefined' === typeof buttons || buttons == null) {
			return;
		}

		//Go through each button
		buttons.forEach(function(button) {

			//Find the link
			var link = button.querySelector('a');
			if (link == null) {
				return;
			}

			//Turn the link into a toggle button, default to closed
			var new_button = document.createElement('button');
			new_button.innerHTML = link.innerHTML;
			new_button.setAttribute('aria-pressed',"false");

			//Change the DOM
			link.remove();
			button.prepend(new_button);

			var menu_locked = false;
			
			//Add event listeners for toggling the button
			new_button.addEventListener('click',function(e) {
				var that = e.currentTarget;
				if (!menu_locked) {
					menu_locked = true;

					var other_count = 0;
					//Remove other submenus
					var other_buttons = container.querySelectorAll('[aria-pressed="true"]');
					//console.log(other_buttons);
					if (other_buttons != null && other_buttons.length > 0) {
						
						other_buttons.forEach(function(button) {
							if (button != that) {
								other_count++;
								button.setAttribute('aria-pressed',"false");
							}
						});
					}

					//Change this button's toggle state
					var ariaPressed = (that.getAttribute('aria-pressed') == "true");

					if (other_count > 0 && !ariaPressed) {

						setTimeout(function() {
							that.setAttribute('aria-pressed',!ariaPressed);
							menu_locked = false;
						}, 300);

					} else {
						menu_locked = false;

						that.setAttribute('aria-pressed',!ariaPressed);
					}
				}
				
			});
			

		});

		//Set a utility class to stop hovering from opening menus
		container.className += " toggle-button-mode";

	});
} )();
