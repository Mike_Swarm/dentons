<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package dentons
 */

get_header();
?>

<div class="main-content">
				<div class="container hero-content">
					<div class="row">
						<div class="col s6 header-copy">
							<h2><span>CHALLENGERS.</span></h2>
							<h2><span>ACCEPTED.</span></h2>
						</div>
						<div class="col s6 intro-copy">
							<h4><span>About us</span></h4>
							<p>Dentons is different. And we’re not trying to disguise it. We’re the world’s largest law firm. We’re the lightbulb moments, the bold ideas, the international deals. We’re the biggest and now we’re set on becoming the best. We’re 10,000+ lawyers and 40+ trainees a year. A truly global, polycentric firm with 80+ languages spoken between us. Redefining the future by moving with innovation and transforming law as we know it. </p>
						</div>
					</div>
				</div>
				<div class="container main-content-area">
				<div class="row">
					<div class="col s12">
					  <ul class="collapsible z-depth-0">
						<li>
						  <div class="collapsible-header">Application form <i class="material-icons">add</i></div>
						  <div class="collapsible-body"><span><p>Your route into Dentons starts with an online form. But, you’re much more than that. This is a chance for you to tell us about your education, work experience and extra-curricular activities. It’s the time to impress us. We will also ask you some long answer questions, to find out about your motivations, drive and attributes that you could bring.</p>

<p>Tips:
<ul><li>Pay attention to the word count: it is there for a reason. If we give you 300 words, that’s how many words we believe you need to give us a detailed answer. </li>
<li>Attention to detail: ask someone to proof read your application. When you’ve spent so much time on something, it can be easy to miss mistakes. </li>
<li>Research: Do your research on Dentons, and be specific. We want to hear what you think makes Dentons unique, and why it’s where you want to be. </li>
<li>Stand out: You’ve covered what makes Dentons unique, but we also want to know about you. What makes you different from the crowd?</li></ul></p>
</span></div>
						</li>
						<li>
						  <div class="collapsible-header">Critical Thinking Test <i class="material-icons">add</i></div>
						  <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
						</li>
						<li>
						  <div class="collapsible-header">Behavioural Interview <i class="material-icons">add</i></div>
						  <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
						</li>
						  <li>
						  <div class="collapsible-header">Assessment Centre <i class="material-icons">add</i></div>
						  <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
						</li>
					  </ul>
					</div>
					</div>
				</div>
				<div style="background-image: url('img/hero1.jpg')" class="hero"></div>

<?php
get_footer();
