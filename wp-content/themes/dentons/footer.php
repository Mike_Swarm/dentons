<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dentons
 */

?>

				<!--Start Footer-->
                <footer class="page-footer">
                    <div class="main-footer-container">
                        <div class="container">
                            <div class="row">
                                <div class="col s12">
                                    <div class="footer-menu">

                                        <?php wp_nav_menu( array( 'menu' => 'Footer' ) ); ?>

                                        <!-- <a href="#">Link</a><a href="#">Link</a><a href="#">Link</a>  -->
                                        


                                    </div>
                                </div>

                                <!-- <div class="col l4 offset-l2 s12">
                                    <div class="footer-menu"> <a href="#">Link</a><a href="#">Link</a><a href="#">Link</a> </div>
                                </div> -->

                            </div>
                        </div>
                    </div>
                    <div class="footer-copyright">
                        <div class="container">

                            <div class="col s12">
                                <p class="copyright">
                                © <?php echo date("Y"); ?>  Dentons. All rights reserved.
                                </p>

                                <p class="social grey-text text-lighten-4 right" href="#!">

                                    <span class="bottom-space">
                                        Be social with us
                                    </span>

                                    <a title="Visit our Facebook" href="https://www.facebook.com/DentonsUKEarlyTalent" target="_blank">
                                        <img alt='Facebook' src="https://challengers.dentons.com/wp-content/themes/dentons/img/Ico-FB.svg">
                                    </a>
                                    <a title="Visit our Twitter" href="https://twitter.com/Dentons" target="_blank">
                                        <img alt="Twitter" src="https://challengers.dentons.com/wp-content/themes/dentons/img/Ico-Twitter.svg">
                                    </a>
                                    <!--<a title="Visit our Instagram" href="https://www.instagram.com" target="_blank">
                                        <img alt="Instagram" src="https://challengers.dentons.com/wp-content/themes/dentons/img/Ico-IG.svg">
                                    </a>-->
                                    <a class="linkedin" title="Visit our Linkedin" href="https://www.linkedin.com/company/dentons/" target="_blank">
                                        <img alt="Linkedin" src="https://challengers.dentons.com/wp-content/themes/dentons/img/Ico-In.svg">
                                    </a>
                                </p>

                            </div>




                        </div>
                    </div>
                </footer>
				<!--End Footer-->
            </div>
        </section>
    </main>
    <div id="cd-loading-bar" data-scale="1" class="index"></div>
    <!--<div class="cursor-dot-outline"></div>
<div class="cursor-dot"></div>-->

    <script src="https://challengers.dentons.com/wp-content/themes/dentons/js/jquery-2.1.4.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="https://challengers.dentons.com/wp-content/themes/dentons/js/velocity.min.js"></script>
    <script src="https://challengers.dentons.com/wp-content/themes/dentons/js/main.js"></script>

<?php wp_footer(); ?>

</body>
</html>
